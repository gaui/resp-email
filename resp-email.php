<?php
    $recipient_email = "contactusfromweb@taeknivit.is"; //recepient
    $from_email      = filter_var($_POST["email"], FILTER_SANITIZE_STRING); //from email using site domain.
    $subject         = filter_var($_POST["subject"] . ' - Skilaboð frá heimasíðu Tæknivits', FILTER_SANITIZE_STRING); //email subject line
    
    $sender_name    = filter_var($_POST["name"], FILTER_SANITIZE_STRING); //capture sender name
    $sender_email   = filter_var($_POST["email"], FILTER_SANITIZE_STRING); //capture sender email
    $sender_message = filter_var($_POST["message"], FILTER_SANITIZE_STRING); //capture message
 
    $headers = "From:" . $from_email . "\r\n" . "Reply-To: " . $sender_email . "\n" . "X-Mailer: PHP/" . phpversion();
    $body    = "from: " . $sender_name .", e-mail: " . $sender_email . "\n \n" . $sender_message;
    
    
    $honeypot = FALSE;
    if (!empty($_REQUEST['full-name-field']) && (bool) $_REQUEST['full-name-field'] == TRUE) {
      $honeypot = TRUE;
      log_spambot($_REQUEST);
    } else {
      $sentMail = @mail($recipient_email, $subject, $body, $headers);
      if ($sentMail) //output success or failure messages
      {
      	// Transfer the value 'sent' to ajax function for showing success message.
      	echo 'sent';
      } else {
      	// Transfer the value 'failed' to ajax function for showing error message.
      	echo 'failed';
      }
    }
?>
